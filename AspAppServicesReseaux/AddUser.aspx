﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="AddUser.aspx.cs" Inherits="AspAppServicesReseaux.AddUser" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPHPageTitle" runat="server">
    Ajout d'utilisateur
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHContent" runat="server">
    <div class="box box-success">
        <div class="box-header">
            <h1 class="box-title">Formulaire d'ajout</h1>
        </div>
        <div class="box-body">
            <form runat="server" class="form-horizontal">
                <div class="form-group">
                    <asp:Label runat="server" Cssclass="col-sm-2 control-label">Nom</asp:Label>
                    <div class="col-sm-10">
                    <asp:TextBox runat="server" Cssclass="form-control"></asp:TextBox>
                    </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" Cssclass="col-sm-2 control-label">Prénom</asp:Label>
                    <div class="col-sm-10">
                    <asp:TextBox runat="server" Cssclass="form-control col-sm-10"></asp:TextBox>
                        </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" Cssclass="col-sm-2 control-label">Login</asp:Label>
                    <div class="col-sm-10">
                    <asp:TextBox runat="server" Cssclass="form-control col-sm-10"></asp:TextBox>
                        </div>
                </div>
                <div class="form-group">
                    <asp:Label runat="server" Cssclass="col-sm-2 control-label">Password</asp:Label>
                    <div class="col-sm-10">
                    <asp:TextBox runat="server" Cssclass="form-control col-sm-10"></asp:TextBox>
                        </div>
                </div>
                    <asp:Button id="SubmitButton1" Text="Ajouter" CssClass="btn btn-success" runat="server" />
                
            </form>
        </div>
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="ContentPlaceHolderJavascript" runat="server">
</asp:Content>
