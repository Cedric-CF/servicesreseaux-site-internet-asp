﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="IndexLivres.aspx.cs" Inherits="AspAppServicesReseaux.IndexLivres" %>

<asp:Content ID="Content1" ContentPlaceHolderID="CPHPageTitle" runat="server">
    Liste des livres
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHContent" runat="server">
    <form id="form1" runat="server">
        
            <asp:Repeater ID="RepeaterLivres" runat="server">
                <HeaderTemplate>

                <div class="row col-md-6 col-xs-12 col-sm-6">
                </HeaderTemplate>
                <ItemTemplate>
                    <div class="box box-info">
                        <div class="box-header">
                            <h3 class="box-title">
                                <%# DataBinder.Eval(Container.DataItem, "DateParution.Year") %>-<%# DataBinder.Eval(Container.DataItem, "Titre") %>
                            </h3>
                        </div>
                        <div class="box-body">
                            <p><strong>Auteur :</strong> <%# DataBinder.Eval(Container.DataItem, "Auteur.Prenom") %>  <%# DataBinder.Eval(Container.DataItem, "Auteur.Nom") %></p>
                            <p><strong>Editeur : </strong><%# DataBinder.Eval(Container.DataItem, "Editeur") %></p>
                            <p><strong>Note :</strong> <%# DataBinder.Eval(Container.DataItem, "Note") %></p>
                            <p><strong>Nombre de pages :</strong> <%# DataBinder.Eval(Container.DataItem, "NombrePages") %></p>
                            <p><strong>ISBN :</strong> <%# DataBinder.Eval(Container.DataItem, "ISBN") %></p>
                        </div>
                    </div>
                </ItemTemplate>
                <FooterTemplate>
                    </div>
               </FooterTemplate>
            </asp:Repeater>
    </form>
</asp:Content>
