﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;


//TODO 1 : Link binding et interaction avec BDD
namespace AspAppServicesReseaux
{
    public partial class AddUser : System.Web.UI.Page
    {
        AspAppServicesReseaux.BiblioWebService.ServiceClient WS;
        protected void Page_Load(object sender, EventArgs e)
        {
            if (this.IsPostBack)
            {
                writeResults(FormSubmit());
            }
            else
            {
                WS = new BiblioWebService.ServiceClient();
                BiblioWebService.Utilisateur user = new BiblioWebService.Utilisateur();
            }
        }

        protected bool FormSubmit()
        {
            AspAppServicesReseaux.BiblioWebService.ServiceClient WS = new BiblioWebService.ServiceClient();
            return WS.addUtilisateur(new AspAppServicesReseaux.BiblioWebService.Utilisateur());
        }
        protected void writeResults(Boolean results)
        {
            if (results == true)
            {
                Response.Write("Success");
            }
            else
            {
                Response.Write("Failed");
            }
        }

    }
}