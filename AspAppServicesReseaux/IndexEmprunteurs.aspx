﻿<%@ Page Title="" Language="C#" MasterPageFile="~/MasterPage.Master" AutoEventWireup="true" CodeBehind="IndexEmprunteurs.aspx.cs" Inherits="AspAppServicesReseaux.IndexEmprunteurs" %>
<asp:Content ID="Content1" ContentPlaceHolderID="CPHPageTitle" runat="server">
    Liste des emprunteurs
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="CPHContent" runat="server">

    <asp:Repeater ID="RepeaterAuteurs" runat="server">
        <HeaderTemplate>
                <div class="box box-info">
        <div class="box-body table-responsive">
	        <table class="table table-bordered table-hover listUsers">
                <thead>
                    <th>Nom</th>
                    <th>Prénom</th>
                    <th>Adresse</th>
                    <th>Email</th>
                    <th>Téléphone</th>
                </thead>
                <tbody>
        </HeaderTemplate>
        <ItemTemplate>
            <tr>
                <td><%# DataBinder.Eval(Container.DataItem, "Nom") %></td>
                <td><%# DataBinder.Eval(Container.DataItem, "Prenom") %></td>
                <td><%# DataBinder.Eval(Container.DataItem, "Adresse") %></td>
                <td><%# DataBinder.Eval(Container.DataItem, "Email") %></td>
                <td><%# DataBinder.Eval(Container.DataItem, "Telephone") %></td>
            </tr>
        </ItemTemplate>
        <FooterTemplate>
                            </tbody>
            </table>
        </div>
    </div>
        </FooterTemplate>
                    </asp:Repeater>


                    
                    

</asp:Content>
<asp:Content ContentPlaceHolderID="ContentPlaceHolderJavascript" ID="Content3" runat="server">
    <script type="text/javascript">
        $(".listUsers").dataTable({
	oLanguage: {
		"sProcessing":     "Traitement en cours...",
		"sSearch":         "Rechercher&nbsp;:",
		"sLengthMenu":     "Afficher _MENU_ &eacute;l&eacute;ments",
		"sInfo":           "Affichage de l'&eacute;lement _START_ &agrave; _END_ sur _TOTAL_ &eacute;l&eacute;ments",
		"sInfoEmpty":      "Affichage de l'&eacute;lement 0 &agrave; 0 sur 0 &eacute;l&eacute;ments",
		"sInfoFiltered":   "(filtr&eacute; de _MAX_ &eacute;l&eacute;ments au total)",
		"sInfoPostFix":    "",
		"sLoadingRecords": "Chargement en cours...",
		"sZeroRecords":    "Aucun &eacute;l&eacute;ment &agrave; afficher",
		"sEmptyTable":     "Aucune donnée disponible dans le tableau",
		"oPaginate": {
			"sFirst":      "Premier",
			"sPrevious":   "Pr&eacute;c&eacute;dent",
			"sNext":       "Suivant",
			"sLast":       "Dernier"
		},
		"oAria": {
			"sSortAscending":  ": activer pour trier la colonne par ordre croissant",
			"sSortDescending": ": activer pour trier la colonne par ordre décroissant"
		}
	}
    }); 
   </script>
 
</asp:Content>