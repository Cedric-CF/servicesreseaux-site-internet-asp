﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace AspAppServicesReseaux
{
    public partial class IndexEmprunteurs : System.Web.UI.Page
    {
        protected void Page_Load(object sender, EventArgs e)
        {
            AspAppServicesReseaux.BiblioWebService.ServiceClient WS = new BiblioWebService.ServiceClient();
            RepeaterAuteurs.DataSource = WS.getAllEmprunteurs();
            RepeaterAuteurs.DataBind();
        }
    }
}